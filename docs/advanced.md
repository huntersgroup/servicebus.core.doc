## Advanced scenarios

In this sections, It will be explained about some scenarios you can cover with this library.

### Implement IDataFormatter

The **IDataFormatter** contract extends **IDataEncoder** and **IDataDecoder**, so incapsulates all these functionalities.

#### Implement IDataEncoder

This contract is responsable to serialize publisher's messages before sending them into ESB brokers.

#### Implement IDataDecoder

This contract is responsable to deserialize data on subscriber side, and let you to retrieve your data encoded by publishers side.

In the following implementation, It's used **Json.Net** to serialize & deserialize the data, and for convenience It's implemented **IDataFormatter** which implements both contracts.

![](img/JsonFormatter.png)

### Implement publishers

As mentioned before, there two different contracts used to send messages, and they will be explained nearby.

#### Implement IPublisher contract

Currently exists an unique implementation for this contract, which uses RabbitMq as ESB Broker, see [Rabbit Provider](rabbit/publishers.md#prepares-publishers)

#### Implement IRestPublisher contract

For this contract, currently there are some implementations, each one dedicated for scopes well defined, [Rabbit Provider - Publishers](rabbit/publishers.md#restpublisher)

### Defining complex RestRequest

Making complex RestRequest could depend how It's complex your application requirements, in the next paragraphs It will be shown some examples about how can be built some complex request.

#### Basic RestRequest with query string

The given request have 2 query string parameters.

```csharp
var req = new RestRequest("/demo?par1=1&par2=2");
```

#### RestRequest with dynamic query parameters

The given request will have query parameters after intialization.
The last parameter "par4" has an encoded value.

```csharp
var req = new RestRequest("api/demo/1/devices");

req.SetQueryParameter("par1", "val1")
    .SetQueryParameter("par2", "val2")
    .SetQueryParameter("par3", "value 9")
    .SetQueryParameter("par4", "value%20C", false);

// if you make an unit test..
Assert.Equal("/api/demo/1/devices?par1=val1&par2=val2&par3=value%209&par4=value%20C", req.Resource);
```

**Note**: In this example the first 3 parameters are exposed to encode its value, instead the last parameter its values is already encoded, so It's not required to apply any encoding.

#### RestRequest with dynamic route parameters

Route parameters are used with placeholders in the resource uri, using brackets.

```csharp
var req = new RestRequest("api/{controller}/1/{property}?myarg1=1&myarg2=2&myarg3=value%209");

req.SetRouteParameter("controller", "demo")
    .SetRouteParameter("property", "devices");

// if you make an unit test..
Assert.Equal("/api/demo/1/devices?myarg1=1&myarg2=2&myarg3=value%209", req.Resource);
```

#### RestRequest with placeholder query parameter

PlaceHolder parameter is used on route and query values, like the following example: 

```csharp
// "par1" is a placeholder query parameter
var req = new RestRequest("api/demo?arg1={par1}");

req.SetQueryPlaceHolder("par1", "myValue");

// if you make an unit test..
Assert.Equal("/api/demo?arg1=myValue", req.Resource);
```

**Note:** Placeholders can be used in route parameters, and query values parameters, if you try to use a placeholder on query parameter name, It will be removed as well.

Example:

```csharp
// "arg2" is a wrong placeholder parameter, so it will be removed.
var req = new RestRequest("api/demo?arg1={par1}&{arg2}=myValue2");

req.SetQueryPlaceHolder("par1", "myValue");

// if you make an unit test..
Assert.Equal("/api/demo?arg1=myValue", req.Resource);
```

If placeholder query parameter isn't set, It will be replaced with default value, so empty string, instead placeholder route parameters must be set, otherwise **an exception will be thrown**, example:

```csharp
var req = new RestRequest("/api/demo/{docId}/devices?myarg={myargNValue}");

// asserts (contains a query place holder not set yet !)
Assert.Throws<UriFormatException>(() => req.Resource);

// if I set the route parameter, no exceptions is thrown.
req.SetRouteParameter("docId", "100");

// fake asserts, but no exception is thrown.
var rex = req.Resource;

// in this case, the placeholder query parameter (myargNValue) is not set, so:
Assert.Equal("/api/demo/100/devices?myarg=", req.Resource);
```