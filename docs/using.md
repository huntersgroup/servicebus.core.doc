# Using ServiceBus.Core

This sections explains the basics of this library. After reading this section, you will have an idea about how you can use this library, and mainly the scope of this one.

Mainly there are two ways which you can use this library:

* **Publishing** (messages) and **Subscribing** (on queues) as well known
* **RPC** approach (Remote Procedure Call)

## Publish & Subscribe mode

### Preparing a message

Regarding the first point, you prepare a simple message to send into ESB / Streaming broker, which can contains surely the right message (**payload**) you intend sending.

Here an simple example of message:

```csharp
// suppose you have defined a class MyCustomPayload

MyCustomPayload payload = PrepareMyPayload(); // returns a payload (the real message to send)
var message = new Message<MyCustomPayload>
{
  Id = "my message id",
  Route = "my route",
  Created = DateTime.Now,
  Ttl = TimeSpan.FromMinutes(30),
  Properties = { {"my custom property", "my custom value"} },
  Data = payload
};

```

The variable message holds the real message to send into broker, and the mandatory or necessary properties to initialize are:

* **Data** - the real message you intend to send into ESB broker.

The other properties could be left with default values, but the real sense for these properties are:

* **Id** - any kind of identifier for the given message
* **Route** - an special property used to apply routing to forward messages.
* **Created** - creation date for this instance
* **Ttl** - Time To Live, used to apply a duration for the given message, in order not to track when time is elapsed.
* **Properties** - custom dictionary which can hold any kind of extra information for this message.

### Send messages

Just having prepared the message, you can publish it using a publisher instance, example:

```csharp
// suppose the MakePublisher method prepares a publisher, so it knows the real implementation.

IPublisher publisher = MakePublisher();
```

```csharp
// sends the message
publisher.Send(message);
```

```csharp
// sends the message in async way (returns a Task)
publisher.SendAsync(message);
```

There are other overloads for these methods, which you can input directly the payload, example:

```csharp
// sends the message
MyCustomPayload payload = PrepareMyPayload();
publisher.Send(payload);
```

```csharp
// sends the message in async way
MyCustomPayload payload = PrepareMyPayload();
publisher.SendAsync(payload);
```

About last examples, **payloads** are held by an **IMessage<TData\>**, pusblishers internally incapsulating your payload into IMessage instances.

## RPC mode
Maybe the usage for this approach could be more interesting for developers, because the behind architecture for this approach follows **a REST style paradigm**.

When I talk about **Rest Style paradigm**, I refer to this one:

![](img/RPC.png)

In the above picture, a publisher sends a message into ESB Broker, then following its routing rules, the message is forwarded into a real queue channel, and after that we have a subscriber listens to the rispective queue channel.

So, in this scenario the microservice (API) acts as Subscriber, so the REST API doesn't only serve HTTP request as well, instead receives messages from ESB Broker, and then transforms messages into Http messages in order to send them into WebAPI endpoints (It could be controllers, or custom routes, depends how it's configured your services).

The most important thing to consider, for existing WebAPIs **it's not required to do anything**, you only need to install a subscriber into **REST API pipeline**, and configure it.

### Preparing a message

The publisher in this case sends a **Rest message** which indicates these informations:

* **endpoint** - partial uri about resource
* **method** - the http method (GET, POST, PUT, DELETE ecc.)
* **payload** - the data to send into destination.

Here some examples about Rest Request.

```csharp
// a Get RestRequest indicating the endpoint
var request = new RestRequest("api/demo")
{
  Method = HttpMethods.Get
};
```

```csharp
// a Post RestRequest indicating the endpoint, method, and payload
var request = new RestRequest("api/demo")
{
  Method = HttpMethods.Post,
  Payload = new { Id = 1, Value = "Custom value 1", Price = 1.5 }
};
```

There are another advanced options for customization of **RestRequest**:

* Set query parameters
* Set route parameters
* Set query placeholders

Once you've prepared your Rest request, you can send it with a publisher.

### Sending messages

In order to send a **RestRequest** message, you need to use an implementation for this interface **IRestPublisher**, example:

```csharp
// suppose the given MakePublisher method creates an instance of IRestPublisher
IRestPublisher publisher = MakePublisher();
```
then you can send the request:

```csharp
// sends the request, without waiting for a Service result.
Task<RestResponse> response = publisher.SendAsync(request);
```

or:

```csharp
// sends the request, waiting for a Service result, in this case you expect an string result.
Task<RestResponse<string>> response = publisher.SendAsync<string>(request);
```

Naturally you can take advantage of async & await keywords:

```csharp
// sends the request, waiting for a Service awaitable result 
RestResponse<string> response = await publisher.SendAsync<string>(request);
```

In the last example, the service response with a string message, but your service can response with any kind of payload, as long as It's a deseralizable data.