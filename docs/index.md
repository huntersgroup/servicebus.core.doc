# Welcome to ServiceBus.Core documentation!

**ServiceBus.Core** is an easy, service oriented, asynchronous message system library, used to communicate with ESB brokers, suitable for any kind of microservice architecture environment.

This library was coded purely in **.Net Standard**, so It supports **NetCore**, **AspNet.Core** frameworks.

Use:

* Get official builds from [NuGet](https://www.nuget.org/packages/ServiceBus.Core/)
* Open Package Manager Console: PM> **Install-package ServiceBus.Core**

Contents:

* [Quick Start](quick-start.md)
* [Using ServiceBus.Core](using.md)
* [Advanced scenarios](advanced.md)

Providers:

* [Using ServiceBus.Core.Rabbit](rabbit/using.md)