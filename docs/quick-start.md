# Quick Start

## Overview

The goal of **ServiceBus.Core** is to provide .NET developers to code easy, fast and flexible way to communicate with service bus systems, providing 
a set of interfaces to implement, and with the same rules in order to standardize messages forwarding.

Another important aspect is the easy way to send messages, these ones could be any kind of type, messages could have simple or complex data structure to send into message brokers without problems, normally data sent to brokers are serialized, so you don't need to worry about serialization, but you can define your own data serialization component in order to apply particular customization for serializing & deserializing data.

Currently exists two kinds of interaction with service bus systems:

### Publishing and subscribing

The most known way to communicate with other microservices is publishing messages (publishers) to recipients (subscribers).

### RPC messaging style

A great strategy which let us to receives reponses from recipients (subscribers) whenever publishers need to receive some output by them.

## Get Started

The easiest way to get started is by installing [the available NuGet packages](https://www.nuget.org/packages/ServiceBus.Core/)
