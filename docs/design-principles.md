## Desing Principles

While designing **ServiceBus.Core**, we decided to apply a set of rules in order to impose a basic set of interfaces which can be implement in the simpliest way,  **keeping semplicity and crushing complexity is not optional, but a requirement for us**.

### Simple contracts, simple implementations

A big overview of this library, this exposes publishers and subscribers with simple methods to implement, defining carefully parameters, keeping agnosticism in order to implement them for specific ESB solutions (ex RabbitMQ, ActiveMQ, Kafka ecc).

### Fast surely, but could depend on your deep knowledges

Fast messages forwarding could depend on your custom client implementation, in many cases depends on ESB capabilities, in the case of RabbitMQ could be easy to implement them, about other clients probably not all contracts could be easily to do, so the best option you have is deep your knowledges about ESB capabilities.

### Clear exceptions on setup clients

Some times libraries (even Microsoft solutions) aren't designed carefully, due to missing good exception management, reporting default exception messages, not usefull for users many times.. so even **this is a requirement for us, and not optional, NEVER**.

Exceptions in most cases are caught by this client, mainly on setup phase when client begins to communicate with Brokers, or maybe when Descriptors report bad or missing  parameters, informing users what happened on setup client with clear exception messages.