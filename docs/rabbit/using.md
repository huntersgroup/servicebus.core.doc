# Using ServiceBus.Core.Rabbit

This sections explains about the usage and implementation for this provider, using [**RabbitMq**](https://www.rabbitmq.com/).

As well known, RabbitMq is a fast, scalable, asynchronous message broker, used in many enterprise solutions widely.

In order to understand well this implementation, It's required a minimal concepts about Rabbit architecture, example about Queues, Brokers, Exchanges, protocolls etc.

## Get Started

The easiest way to get started is by installing [the available NuGet packages](https://www.nuget.org/packages/ServiceBus.Core.Rabbit/)

## Features

### No RabbitMQ configuration
It's not mandatory configure your RabbitMQ components (Queues, Exchanges, Binding ecc) and other resources as needed, because this library does it for you using its own **descriptors**.

### Asynchronous Messaging
Sending and receiving messages works in async way.

### Easy horizontal scaling
Scaling is the easiest task to do, because you need to create new Subcribers (new WebAPIs, or batch deployments) in order to increase load balancing capabilities.

### Use your existing REST microservices
Your current microservices could work as well via **HTTP** protocol, at the same time could work as Subscribers, so consuming messages listening **RabbitMQ queues**.
With a little lines of code, you can transform your Web APIs in **Message Oriented Architecture**.

### Customizable
Some components could be re-implemented, depending of use cases, example:

* **IDataFormatter** - responsable to serializer and deserialize data
* **IHttpRestMessageSerializer** - responsable to read & write Http Rest Messages into streams

## How It works

* [Publishers](publishers.md)
* [Subscribers](subscribers.md)
