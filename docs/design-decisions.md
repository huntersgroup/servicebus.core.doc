## Design Decisions

Our design principles have imposed us to develop in a standard way, valid for any kind of ESB solutions, with simple contracts, and exposing some basic / valid implementations for the most part of ESB technologies on market.

### Each client has one descriptor

As mentioned in another part of this tutorial, every publisher and subscriber has its own descriptor, and in the most cases they inherit from other ones.

### Clients are descriptive

As explained on descriptor section, clients can be configured only by descriptors.

### Descriptors only describe your Clients

Descriptors are simple POCO classes, less of any kind of logic, just it should be done !

### Descriptors are easy to setup

Descriptors can be setup on your configuration file (appsettings.json), and this is the best and recommended way to follow.

### Publishing and subscribing in async way

Whenever is possible, you need to consider async tasks, It could be improve your application workflows, but It's not mandatary surely.

### Publishers can hold any kind of messages

Publishers are agnostic, they could hold any kind of message, even you can add custom properties to send into subscribers.

There are others properties related to the channel, so you can take advantage of this customization sending extra info (example message routing, TTL etc).

### RPC clients work in a duplex way

In this case, publishers are designed in a simple way to comunicate with Rest subscribers, knowing and indicating in a secure way which is the callback queue which will be sent the response by Rest Subscribers.

### Rest clients have dedicated Reply queues

All client responses sent by subscribers aren't mixed with other client responses, so each RestClient will have one dedicated callback queue (**exclusive**), not shared by other RestClients and not other processes.